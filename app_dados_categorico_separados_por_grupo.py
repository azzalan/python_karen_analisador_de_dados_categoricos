import tkinter as tk
from tkinter import filedialog, simpledialog, messagebox
import pandas as pd

def analyze_excel(file_path, header_row_index, column_index):
    # Load the Excel file
    df = pd.read_excel(file_path, header=header_row_index)
    column_to_analyze = df.columns[column_index]
    # Get the unique categories from the specified column
    unique_categories = df[column_to_analyze].unique()
    
    # Prepare the output DataFrame
    output_data = []

    # Iterate through each unique header and its unique categories
    for header in df.columns:
        if header != column_to_analyze:
            # Retrieve unique categories for the current header
            header_categories = df[header].unique()
            for header_category in header_categories:
                # For each category from the column to analyze, calculate occurrences and frequency
                for category in unique_categories:
                    category_mask = df[column_to_analyze] == category
                    occurrences = sum(df[header][category_mask] == header_category)
                    frequency = occurrences / sum(category_mask) * 100
                    output_data.append([header, category, header_category, occurrences, frequency])

    # Convert the list to a DataFrame
    output_df = pd.DataFrame(output_data, columns=[
        'Header', column_to_analyze, 'Category', 'Occurrences', 'Frequency (%)'])

    # Save the output DataFrame to a new Excel file
    output_path = file_path.rsplit('.', 1)[0] + '_categorized.xlsx'
    output_df.to_excel(output_path, index=False)
    
    messagebox.showinfo("Success", f"Output saved to {output_path}")

def prompt_for_header_and_column(file_path):
    header_row_index = simpledialog.askinteger("Input", "Escreva qual o número da linha em que está o cabeçalho (1 para a primeira linha):", minvalue=1)
    if header_row_index is not None:
        header_row_index -= 1  # Convert to zero-based index
        df = pd.read_excel(file_path, header=None)  # Temporarily load without headers to get column labels
        column_names = df.iloc[header_row_index].tolist()
        column_index = simpledialog.askinteger("Input", f"Escreva que o número da coluna é que categoriza os grupos:", minvalue=1, maxvalue=len(column_names))
        if column_index is not None:
            analyze_excel(file_path, header_row_index, column_index - 1)  # Adjust for zero-based index

def select_file():
    file_path = filedialog.askopenfilename(filetypes=[("Excel files", "*.xlsx")])
    if file_path:
        prompt_for_header_and_column(file_path)

# Tkinter GUI setup
root = tk.Tk()
root.title("Excel Categorical Data Analyzer")

# Button to select the Excel file
select_file_btn = tk.Button(root, text="Select Excel File", command=select_file)
select_file_btn.pack(pady=20)

root.mainloop()
