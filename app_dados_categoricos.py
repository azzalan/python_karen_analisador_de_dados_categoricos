import tkinter as tk
from tkinter import filedialog, simpledialog
import pandas as pd

def analyze_excel(header_row):
    # Load the Excel file
    df = pd.read_excel(file_path, header=header_row-1)  # Adjusting for zero-based indexing
    total_rows = len(df)

    # Prepare the output DataFrame
    output_data = []

    for column in df.columns:
        # Get the frequency of each category in the column
        counts = df[column].value_counts()
        frequencies = df[column].value_counts(normalize=True) * 100  # Convert to percentage

        for category, count in counts.items():
            frequency = frequencies[category]
            output_data.append([column, category, count, frequency])

    # Convert output data to DataFrame
    output_df = pd.DataFrame(output_data, columns=['Header', 'Category', 'Occurrences', 'Frequency (%)'])

    # Save the output DataFrame to a new Excel file
    output_path = file_path.rsplit('.', 1)[0] + '_categorized.xlsx'
    output_df.to_excel(output_path, index=False)
    print(f"Output saved to {output_path}")

def select_file():
    global file_path
    file_path = filedialog.askopenfilename(filetypes=[("Excel files", "*.xlsx")])
    if file_path:
        header_row = simpledialog.askinteger("Input", "Enter the row index for the header:", minvalue=1)
        if header_row is not None:
            analyze_excel(header_row)

# Tkinter GUI setup
root = tk.Tk()
root.title("Excel Categorical Data Analyzer")

# Button to select the Excel file
select_file_btn = tk.Button(root, text="Select Excel File", command=select_file)
select_file_btn.pack(pady=20)

root.mainloop()
