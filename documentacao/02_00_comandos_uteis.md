#### Comandos úteis

##### Resolver problema: Pylance (reportMissingImport)

- Dentro do vscode, abra o command palette com `CTRL + SHIFT + P`
- Procure por `Python: Select Interpreter`
- Selecione a opção que indique `PipEnv` e inclua o nome da pasta que você está trabalhando
