#### Instalando - primeiros passos

- Instalar python no windows
- Instalar git no windows
- Configurar sua autenticação no git por ssh
- Abrir powershell como administrador

```
Tecla do windows
Escreva "powershell"
Click na opção "Executar como administrador"

```

- Instalar pacote que permite usar gerenciar pacotes usando ambientes virtuais com maior facilidade

```
pip install pipenv

```

- Navegar uma pasta pai onde você deseja guardar a pasta do projeto projeto, usaremos `C:/_code` como referência para o resto da documentação.

```
cd C:/_code
```

- Baixar arquivos do projeto

```
git clone https://gitlab.com/azzalan/python_karen_tratar_tabela_de_actstudio_sono_media.git
```

- Navegar para a pasta que acabou de ser criada

```
cd python_karen_tratar_tabela_de_actstudio_sono_media
```

- Instalar os pacotes básicos do projeto.

```
pipenv install
```

- Crie uma pasta chamada `input` dentro do projeto, se ela não existir

- Crie outra pasta chamada `output`, se ela não existir

- Para usar o scripts coloque os tabelas a serem processadas dentro da pasta `input`

- Rode o script usando o comando

```
pipenv run .\script.py
```
